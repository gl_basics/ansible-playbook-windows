# Ansible playbook for windows
What it does:
- Runs ansible playbook against windows servers and uses ansible "assert" to verify changes were made. 

Setup:
- Shell runner on ubuntu server.
- Ansible control host with win_vars file secured on server.
  - win_vars file contains windows service account username and password. 
- CICD variables for ansible host user and ssh key.

Sequence:
- Deploy
  - Secures ssh private key for ansible host login.
  - Copies ansible files to ansible control host.
  - Logs into ansible control host and runs playbook.
- Test
  - Secures ssh private key for ansible host login.
  - Copies test (assert) file to ansible control host.
  - Runs assert playbook to verify changes are in place on target windows servers. 
